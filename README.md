# JetBrains HTTP Client as a replacement for Poopman

Postman become a bloated crap. It used to be a slick Chrome Extension app back in the days, not a heavy Electron app.
That's how it became widely used and respected tool in the past.
Yet, regards the decreased user experience, it remained the best application on the market.

Another feature is CLI runner called Newman. It allows us to run Postman collection in CI without graphical interface.

The worst behaviour of Postman is the format of exported collection. It's in JSON, that's very hard readable or
editable.
The only sane option is use Postman GUI app for edit.

Another big concern is about their push to use of logged account that sends and stores executed requests in their cloud.
That's a big no no.

The great alternative to this is HTTP Client included in any JetBrains IDE.
It can run HTTP requests from `<name>.http` files.

Documentation can be found [Here](https://www.jetbrains.com/help/idea/http-client-in-product-code-editor.html)

Yet, we didn't have any alternative to the Postman's Newmanfeature, CLI
runner. [Not until now.](https://blog.jetbrains.com/idea/2022/12/http-client-cli-run-requests-and-tests-on-ci/)

There are some obstacles in the adpoption/migration though.

One
is [conversion of our Postman test collectons](https://lengrand.fr/replacing-postman-in-seconds-with-the-jetbrains-http-client/).
There is no automation,
and [JetBrains isn't working on it yet](https://youtrack.jetbrains.com/issue/IDEA-312760/HTTP-Client-ability-to-convert-Postman-collections-to-.http-files).

The request could be converted by exporting Postman request to `curl` request, and being pasted to the `*.http` file.
The request will ge converted by your IDE. One by one, unfortunately.

Another obstacle is different request response evaluation.
Postman uses [ChaiJS BDD](https://www.chaijs.com/api/bdd/) framework
for [validating responses](https://learning.postman.com/docs/writing-scripts/test-scripts/#validating-responses).

While JetBrains HTTP client uses its own, incompatible, assertion library.
It should
be [able to import external modules](https://youtrack.jetbrains.com/issue/IDEA-287544/http.file-when-trying-to-import-function-from-js.file-get-an-assert-Expected-an-operand-but-found-import-import-check-from#focus=Comments-27-7335735.0-0),
but I didn't manage to make it work.

The big advantage of JetBrains HTTP client is simple syntax that can be easily modified and run in IDE and tracked in
git.

It also has limited support of WebSocket or gRPC. Meanwhile, Postman supports gRPC only if you logged-in, and you share
your requests with Poopman Inc.

BTW, there
is [experimental OpenAPI => JetBrains HTTP Client convertor/generator](https://openapi-generator.tech/docs/generators/jetbrains-http-client)

![img](how-to-run.png)